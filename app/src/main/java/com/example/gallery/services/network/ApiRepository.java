package com.example.gallery.services.network;

import android.util.Log;

import com.example.gallery.services.listeners.GetDetailsListener;
import com.example.gallery.services.listeners.GetPhotosListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.example.gallery.data.ImageData;
import com.example.gallery.data.ImageResponseData;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

@Singleton
public class ApiRepository {
    private ApiService apiService;

    @Inject
    public ApiRepository(ApiService apiService) {
        this.apiService = apiService;
    }

    public void getPhotos(final GetPhotosListener listener, int page, int per_page) {
        apiService.getPhotos(page, per_page).enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                List<ImageData> imagesData = new ArrayList<>();
                JsonArray images = response.body();
                if (images != null) {
                    for (int i = 0; i < images.size(); i++) {
                        JsonObject imageObject = images.get(i).getAsJsonObject();
                        ImageData imageData = parseImageData(imageObject);
                        imagesData.add(imageData);
                    }
                    listener.onImagesReady(imagesData);
                } else {
                    String error = response.errorBody().toString();
                    listener.onFail(error);
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                if (t instanceof UnknownHostException || t instanceof SocketTimeoutException) {
                    listener.onInternetUnavailable();
                } else {
                    listener.onFail(t.getLocalizedMessage());
                }
            }
        });
    }

    private ImageData parseImageData(JsonObject jsonObject) {
        ImageData imageData = new ImageData();

        if (!jsonObject.isJsonNull()) {
            if (!jsonObject.get("id").isJsonNull()) {
                String id = jsonObject.get("id").getAsString();
                imageData.setId(id);
            }

            if (!jsonObject.getAsJsonObject("urls").isJsonNull()) {
                JsonObject urls = jsonObject.getAsJsonObject("urls");
                for (int i = 0; i < urls.size(); i++) {
                    if (!urls.get("small").isJsonNull()) {
                        String url = urls.get("small").getAsString();
                        imageData.setRemoteUrl(url);
                    }
                }
            }
        }

        return imageData;
    }

    public void getDetails(final GetDetailsListener listener, ImageData imageData) {
        apiService.getDetails(imageData.getId()).enqueue(new Callback<ImageResponseData>() {
            @Override
            public void onResponse(Call<ImageResponseData> call, Response<ImageResponseData> response) {
                ImageResponseData imageResponseData = response.body();
                if (imageResponseData != null) {
                    if (imageResponseData.getAltDescription() != null) {
                        imageData.setDescription(imageResponseData.getAltDescription());
                    }

                    if (imageResponseData.getCreatedAt() != null) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
                        try {
                            Date date = dateFormat.parse(imageResponseData.getCreatedAt());
                            imageData.setDate(date);
                        } catch (Exception e) {
                            Log.i(TAG, "parseImageData: " + e.getLocalizedMessage());
                        }
                    }

                    if (imageResponseData.getUser() != null) {
                        if (imageResponseData.getUser().getName() != null) {
                            imageData.setUsername(imageResponseData.getUser().getName());
                        }

                        if (imageResponseData.getUser().getProfileImage() != null) {
                            if (imageResponseData.getUser().getProfileImage().getLarge() != null) {
                                imageData.setProfileImage(imageResponseData.getUser().getProfileImage().getLarge());
                            }
                        }

                        if (imageResponseData.getUser().getPortfolioUrl() != null) {
                            imageData.setUserPortfolioLink(imageResponseData.getUser().getPortfolioUrl());
                        }
                    }
                }
                listener.onSuccess(imageData);
            }

            @Override
            public void onFailure(Call<ImageResponseData> call, Throwable t) {
                listener.onFail(t.getLocalizedMessage());
            }
        });
    }
}
