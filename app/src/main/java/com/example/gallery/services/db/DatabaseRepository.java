package com.example.gallery.services.db;

import android.os.AsyncTask;

import com.example.gallery.App;
import com.example.gallery.data.ImageData;
import com.example.gallery.services.listeners.GetLocalIdListener;
import com.example.gallery.services.listeners.GetPhotosListener;
import com.example.gallery.services.listeners.SuccessListener;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DatabaseRepository {
    private ImageDao imageDao;
    @Inject
    AppDatabase database;

    @Inject
    public DatabaseRepository() {
        App.getAppComponent().inject(this);
        imageDao = database.imageDao();
    }

    public void insert(ImageData imageData, GetLocalIdListener listener) {
        new InsertImageAsyncTask(imageDao, listener).execute(imageData);
    }

    public void delete(ImageData imageData, SuccessListener listener) {
        new DeleteImageAsyncTask(imageDao, listener).execute(imageData);
    }

    public void getAll(GetPhotosListener listener) {
        new GetAllImagesAsyncTask(imageDao, listener).execute();
    }

    private static class InsertImageAsyncTask extends AsyncTask<ImageData, Void, Void> {
        private ImageDao imageDao;
        private GetLocalIdListener listener;
        private long id;


        public InsertImageAsyncTask(ImageDao imageDao, GetLocalIdListener listener) {
            this.imageDao = imageDao;
            this.listener = listener;
        }

        @Override
        protected Void doInBackground(ImageData... imageData) {
            id = imageDao.insert(imageData[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onSuccess(id);
        }
    }

    private static class DeleteImageAsyncTask extends AsyncTask<ImageData, Void, Void> {
        private ImageDao imageDao;
        private SuccessListener listener;

        public DeleteImageAsyncTask(ImageDao imageDao, SuccessListener listener) {
            this.imageDao = imageDao;
            this.listener = listener;
        }

        @Override
        protected Void doInBackground(ImageData... imageData) {
            imageDao.delete(imageData[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onSuccess();
        }
    }

    private static class GetAllImagesAsyncTask extends AsyncTask<Void, Void, List<ImageData>> {
        private ImageDao imageDao;
        private GetPhotosListener listener;

        public GetAllImagesAsyncTask(ImageDao imageDao, GetPhotosListener listener) {
            this.imageDao = imageDao;
            this.listener = listener;
        }

        @Override
        protected List<ImageData> doInBackground(Void... voids) {
            return imageDao.getAll();
        }

        @Override
        protected void onPostExecute(List<ImageData> imageData) {
            super.onPostExecute(imageData);
            listener.onImagesReady(imageData);
        }
    }

}
