package com.example.gallery.services;

import com.example.gallery.data.ImageData;
import com.example.gallery.services.db.DatabaseRepository;
import com.example.gallery.services.listeners.GetPhotosListener;
import com.example.gallery.services.listeners.SuccessListener;
import com.example.gallery.services.network.ApiRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class ImagesInteractor {
    private ApiRepository apiRepository;
    private DatabaseRepository databaseRepository;
    private ImagesService imagesService;
    private boolean isFavoriteInProgress = false;

    @Inject
    public ImagesInteractor(ApiRepository apiRepository, DatabaseRepository databaseRepository, ImagesService imagesService) {
        this.apiRepository = apiRepository;
        this.databaseRepository = databaseRepository;
        this.imagesService = imagesService;
    }

    public void getImages(GetPhotosListener getPhotosListener) {
        apiRepository.getPhotos(new GetPhotosListener() {
            @Override
            public void onImagesReady(List<ImageData> images) {
                apiRepository.getPhotos(new GetPhotosListener() {
                    @Override
                    public void onImagesReady(List<ImageData> imgs) {
                        List<ImageData> imageData = new ArrayList<>();
                        imageData.addAll(images);
                        imageData.addAll(imgs);
                        getFavoriteImages(new GetPhotosListener() {
                            @Override
                            public void onImagesReady(List<ImageData> dbImages) {
                                if (dbImages.size() > 0) {
                                    for (ImageData mainImage : imageData) {
                                        for (ImageData favoriteImage : dbImages) {
                                            if (mainImage.getId().equals(favoriteImage.getId())) {
                                                mainImage.setIdLocal(favoriteImage.getIdLocal());
                                                mainImage.setFavorite(favoriteImage.isFavorite());
                                                mainImage.setLocalUrl(favoriteImage.getLocalUrl());
                                            }
                                        }
                                    }
                                }
                                getPhotosListener.onImagesReady(imageData);
                            }

                            @Override
                            public void onFail(String error) {
                                getPhotosListener.onFail(error);
                            }

                            @Override
                            public void onInternetUnavailable() {

                            }
                        });
                    }

                    @Override
                    public void onFail(String error) {
                        getPhotosListener.onFail(error);
                    }

                    @Override
                    public void onInternetUnavailable() {
                        getPhotosListener.onInternetUnavailable();
                    }
                }, 5, 20);
            }

            @Override
            public void onFail(String error) {
                getPhotosListener.onFail(error);
            }

            @Override
            public void onInternetUnavailable() {
                getPhotosListener.onInternetUnavailable();
            }
        }, 1, 30);
    }

    public void getFavoriteImages(GetPhotosListener listener) {
        databaseRepository.getAll(listener);
    }

    public void addToFavorites(ImageData imageData, SuccessListener listener) {
        if (isFavoriteInProgress) {
            return;
        }
        isFavoriteInProgress = true;
        imageData.setFavorite(true);
        imagesService.saveImage(imageData, () -> databaseRepository.insert(imageData, id -> {
            imageData.setIdLocal(id);
            isFavoriteInProgress = false;
            listener.onSuccess();
        }));
    }

    public void removeFromFavorites(ImageData imageData, SuccessListener listener) {
        if (isFavoriteInProgress) {
            return;
        }
        isFavoriteInProgress = true;
        imageData.setFavorite(false);
        imagesService.removeImage(imageData);
        databaseRepository.delete(imageData, () -> {
            isFavoriteInProgress = false;
            listener.onSuccess();
        });
    }
}
