package com.example.gallery.services.network;

import com.google.gson.JsonArray;
import com.example.gallery.data.ImageResponseData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("photos?client_id=09d11e7a021a5dcaf29f7eacc04d85cf11c5e979f3c40b2bd3fc43c7d6d2c028")
    Call<JsonArray> getPhotos(@Query("page") int page, @Query("per_page") int per_page);

    @GET("photos/{id}/?client_id=09d11e7a021a5dcaf29f7eacc04d85cf11c5e979f3c40b2bd3fc43c7d6d2c028")
    Call<ImageResponseData> getDetails(@Path("id") String id);
}
