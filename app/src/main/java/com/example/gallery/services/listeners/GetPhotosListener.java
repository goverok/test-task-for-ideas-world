package com.example.gallery.services.listeners;

import com.example.gallery.data.ImageData;

import java.util.List;

public interface GetPhotosListener {
    void onImagesReady(List<ImageData> images);

    void onFail(String error);

    void onInternetUnavailable();
}
