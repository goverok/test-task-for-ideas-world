package com.example.gallery.services.listeners;

import com.example.gallery.data.ImageData;

public interface GetDetailsListener {
    void onSuccess(ImageData imageData);

    void onFail(String error);
}
