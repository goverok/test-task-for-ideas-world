package com.example.gallery.services.listeners;

public interface GetLocalIdListener {
    void onSuccess(long id);
}
