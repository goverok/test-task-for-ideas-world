package com.example.gallery.services.listeners;

public interface SuccessListener {
    void onSuccess();
}
