package com.example.gallery.services.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.gallery.data.ImageData;

import java.util.List;

@Dao
public interface ImageDao {
    @Query("SELECT * FROM image")
    List<ImageData> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(ImageData imageData);

    @Delete
    void delete(ImageData imageData);
}
