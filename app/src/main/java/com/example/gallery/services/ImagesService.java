package com.example.gallery.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.gallery.data.ImageData;
import com.example.gallery.services.listeners.SuccessListener;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ImagesService {
    private Context context;

    @Inject
    public ImagesService(Context context) {
        this.context = context;
    }

    public void displayImage(ImageData imageData, ImageView mainImage) {
        if (mainImage != null) {
            if (imageData.getLocalUrl() != null) {
                Glide.with(context)
                        .load(new File(imageData.getLocalUrl()))
                        .into(mainImage);
            } else if (imageData.getRemoteUrl() != null) {
                Glide.with(context)
                        .asBitmap()
                        .load(imageData.getRemoteUrl())
                        .into(mainImage);
            }
        }
    }

    public void displayUserProfileImage(ImageData imageData, ImageView imageView) {
        Glide.with(context)
                .load(imageData.getProfileImage())
                .into(imageView);
    }

    public void displayImage(int id, ImageView imageView) {
        Glide.with(context)
                .load(id)
                .into(imageView);
    }

    public void saveImage(ImageData imageData, SuccessListener listener) {
        Glide.with(context)
                .asBitmap()
                .load(imageData.getRemoteUrl())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        imageData.saveImage(resource, context);
                        listener.onSuccess();
                    }
                });
    }

    public void removeImage(ImageData imageData) {
        imageData.deleteImageFromStorage();
    }
}
