package com.example.gallery.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

@Entity(tableName = "image")
public class ImageData {
    @PrimaryKey(autoGenerate = true)
    private long idLocal;
    private String id;
    private String remoteUrl;
    private String localUrl;
    private boolean isFavorite;
    private Date date;
    private String description;
    private String username;
    private String profileImage;
    private String userPortfolioLink;

    public ImageData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemoteUrl() {
        return remoteUrl;
    }

    public void setRemoteUrl(String remoteUrl) {
        this.remoteUrl = remoteUrl;
    }

    public String getLocalUrl() {
        return localUrl;
    }

    public void setLocalUrl(String localUrl) {
        this.localUrl = localUrl;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void saveImage(Bitmap image, Context context) {
        String savedImagePath = null;
        String imageFileName = id + ".jpg";
        ContextWrapper cw = new ContextWrapper(context);
        File folder = new File(cw.getFilesDir() + "/Images");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            File imageFile = new File(folder, imageFileName);
            savedImagePath = imageFile.getAbsolutePath();
            try {
                OutputStream fOut = new FileOutputStream(imageFile);
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        localUrl = savedImagePath;
    }

    public void deleteImageFromStorage() {
        if (localUrl != null) {
            File file = new File(localUrl);
            file.delete();
            localUrl = null;
        }
    }

    public long getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(long idLocal) {
        this.idLocal = idLocal;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getUserPortfolioLink() {
        return userPortfolioLink;
    }

    public void setUserPortfolioLink(String userPortfolioLink) {
        this.userPortfolioLink = userPortfolioLink;
    }
}
