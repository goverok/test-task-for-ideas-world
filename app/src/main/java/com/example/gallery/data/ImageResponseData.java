package com.example.gallery.data;

import com.google.gson.annotations.SerializedName;

public class ImageResponseData {
    private String id;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("alt_description")
    private String altDescription;
    private User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getAltDescription() {
        return altDescription;
    }

    public User getUser() {
        return user;
    }

    public class User {

        private String name;
        @SerializedName("profile_image")
        private ProfileImage profileImage;
        @SerializedName("portfolio_url")
        private String portfolioUrl;

        public String getName() {
            return name;
        }

        public ProfileImage getProfileImage() {
            return profileImage;
        }

        public String getPortfolioUrl() {
            return portfolioUrl;
        }

        public class ProfileImage {

            private String large;

            public String getLarge() {
                return large;
            }
        }

    }
}
