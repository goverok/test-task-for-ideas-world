package com.example.gallery.di;

import com.example.gallery.services.db.DatabaseRepository;
import com.example.gallery.ui.favorite.FavoritesFragment;
import com.example.gallery.ui.info.InfoFragment;
import com.example.gallery.ui.photo.ImageFragment;
import com.example.gallery.ui.photo.ImagesFragment;
import com.example.gallery.ui.photo.PhotoAdapter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(ImagesFragment imagesFragment);

    void inject(ImageFragment imageFragment);

    void inject(InfoFragment infoFragment);

    void inject(FavoritesFragment favoritesFragment);

    void inject(PhotoAdapter photoAdapter);

    void inject(DatabaseRepository databaseRepository);
}
