package com.example.gallery.ui;

import android.Manifest;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rbddevs.splashy.Splashy;
import com.example.gallery.R;
import com.example.gallery.ui.favorite.FavoritesFragment;
import com.example.gallery.ui.info.InfoFragment;
import com.example.gallery.ui.photo.ImagesFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.navigationView)
    BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSplashScreen();
        requestPermissions();
        ButterKnife.bind(this, this);

        navigationView.setSelectedItemId(R.id.navigation_photo);
        navigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.navigation_favorite:
                    onFavoriteButtonClick();
                    break;
                case R.id.navigation_photo:
                    onPhotoButtonClick();
                    break;
                case R.id.navigation_Info:
                    onInfoButtonClick();
            }
            return true;
        });
    }

    private void onInfoButtonClick() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new InfoFragment(), "info_fragment")
                .commit();
    }

    private void onPhotoButtonClick() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new ImagesFragment(), "images_fragment")
                .commit();
    }

    private void onFavoriteButtonClick() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new FavoritesFragment(), "favorites_fragment")
                .commit();

    }

    private void setSplashScreen() {
        new Splashy(this)
                .setLogo(R.mipmap.ic_launcher)
                .setTitle("")
                .show();
    }

    private void requestPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            onPhotoButtonClick();
                        } else {
                            finish();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(error -> this.showToastMessage(error.name()))
                .onSameThread()
                .check();
    }

    private void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
