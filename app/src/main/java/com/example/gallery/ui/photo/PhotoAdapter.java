package com.example.gallery.ui.photo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.gallery.App;
import com.example.gallery.R;
import com.example.gallery.data.ImageData;
import com.example.gallery.services.ImagesService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ImageData> imagesData;
    private LayoutInflater inflater;
    private FragmentManager manager;
    private FavoritesListener listener;


    @Inject
    ImagesService imagesService;

    public PhotoAdapter(Context context, FragmentManager manager, FavoritesListener listener) {
        imagesData = new ArrayList<>();
        inflater = LayoutInflater.from(context);
        this.manager = manager;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.grid_item, viewGroup, false);
        final GridViewAdapterViewHolder viewHolder = new GridViewAdapterViewHolder(view);
        App.getAppComponent().inject(this);

        view.setOnClickListener(v -> {
            ImageData image = imagesData.get(viewHolder.getAdapterPosition());
            manager.beginTransaction()
                    .replace(R.id.container, ImageFragment.newInstance(image))
                    .addToBackStack("image_fragment")
                    .commit();
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((GridViewAdapterViewHolder) viewHolder).bind(imagesData.get(i));
    }

    @Override
    public int getItemCount() {
        return imagesData.size();
    }

    public class GridViewAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageItem)
        ImageView imageItem;
        @BindView(R.id.favoriteImage)
        ImageView favoriteImage;

        public GridViewAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.findViewById(R.id.item);
            ButterKnife.bind(this, itemView);
        }

        void bind(final ImageData current) {
            imagesService.displayImage(current, imageItem);
            imagesService.displayImage(current.isFavorite() ? R.drawable.ic_favorite_red_24dp : R.drawable.ic_favorite_border_black_24dp, favoriteImage);
            favoriteImage.setOnClickListener(v -> {
                if (current.isFavorite()) {
                    current.setFavorite(false);
                    listener.onFavoritePressed(false, current);
                } else {
                    current.setFavorite(true);
                    listener.onFavoritePressed(true, current);
                }
                imagesService.displayImage(current.isFavorite() ? R.drawable.ic_favorite_red_24dp : R.drawable.ic_favorite_border_black_24dp, favoriteImage);
            });
        }
    }

    public List<ImageData> getImagesData() {
        return imagesData;
    }

    public void setImagesData(List<ImageData> imagesData) {
        this.imagesData = imagesData;
        notifyDataSetChanged();
    }

    public void deleteFavoriteImage(ImageData imageData) {
        int i = imagesData.indexOf(imageData);
        imagesData.remove(imageData);
        notifyItemRemoved(i);
    }

    public interface FavoritesListener {
        void onFavoritePressed(boolean status, ImageData imageData);
    }

}
