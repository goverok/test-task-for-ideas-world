package com.example.gallery.ui.favorite;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.gallery.App;
import com.example.gallery.R;
import com.example.gallery.data.ImageData;
import com.example.gallery.services.ImagesInteractor;
import com.example.gallery.services.listeners.GetPhotosListener;
import com.example.gallery.ui.photo.PhotoAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoritesFragment extends Fragment implements PhotoAdapter.FavoritesListener {
    @BindView(R.id.favoritesView)
    RecyclerView favoritesView;

    @Inject
    ImagesInteractor imagesInteractor;
    private PhotoAdapter photoAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this, view);

        getImagesFromDb();
        initGridView();

        return view;
    }

    private void initGridView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        favoritesView.setLayoutManager(gridLayoutManager);
        photoAdapter = new PhotoAdapter(getContext(), getFragmentManager(), this);
        favoritesView.setAdapter(photoAdapter);
    }

    private void getImagesFromDb() {
        imagesInteractor.getFavoriteImages(new GetPhotosListener() {
            @Override
            public void onImagesReady(List<ImageData> images) {
                photoAdapter.setImagesData(images);
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInternetUnavailable() {

            }
        });
    }

    @Override
    public void onFavoritePressed(boolean status, ImageData imageData) {
        if (!status) {
            imagesInteractor.removeFromFavorites(imageData, () -> photoAdapter.deleteFavoriteImage(imageData));
        }
    }
}
