package com.example.gallery.ui.photo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gallery.App;
import com.example.gallery.R;
import com.example.gallery.data.ImageData;
import com.example.gallery.services.ImagesInteractor;
import com.example.gallery.services.listeners.GetPhotosListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagesFragment extends Fragment implements PhotoAdapter.FavoritesListener {
    @BindView(R.id.photoView)
    RecyclerView photoView;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @BindView(R.id.internetConnectionText)
    TextView internetConnectionText;

    private PhotoAdapter photoAdapter;
    @Inject
    ImagesInteractor imagesInteractor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_images, container, false);
        ButterKnife.bind(this, view);

        initGridView();
        getImages();

        swipeContainer.setOnRefreshListener(this::getImages);
        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
        return view;
    }

    private void initGridView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        photoView.setLayoutManager(gridLayoutManager);
        photoAdapter = new PhotoAdapter(getContext(), getFragmentManager(), this);
        photoView.setAdapter(photoAdapter);
    }

    private void getImages() {
        imagesInteractor.getImages(new GetPhotosListener() {
            @Override
            public void onImagesReady(List<ImageData> images) {
                photoView.setVisibility(View.VISIBLE);
                internetConnectionText.setVisibility(View.GONE);
                photoAdapter.setImagesData(images);
                swipeContainer.setRefreshing(false);
            }

            @Override
            public void onFail(String error) {
                swipeContainer.setRefreshing(false);
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInternetUnavailable() {
                swipeContainer.setRefreshing(false);
                internetConnectionText.setVisibility(View.VISIBLE);
                photoView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onFavoritePressed(boolean status, ImageData imageData) {
        if (status) {
            imagesInteractor.addToFavorites(imageData, () -> photoAdapter.notifyDataSetChanged());
        } else {
            imagesInteractor.removeFromFavorites(imageData, () -> {
                int i = photoAdapter.getImagesData().indexOf(imageData);
                photoAdapter.notifyItemChanged(i);
            });
        }

    }
}
