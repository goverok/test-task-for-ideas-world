package com.example.gallery.ui.info;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gallery.App;
import com.example.gallery.R;
import com.example.gallery.services.ImagesService;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InfoFragment extends Fragment {
    @Inject
    ImagesService imagesService;

    @BindView(R.id.profileImage)
    ImageView profileImage;
    @BindView(R.id.firstColumn)
    TextView firstColumn;
    @BindView(R.id.secondColumn)
    TextView secondColumn;
    @BindView(R.id.thirdColumn)
    TextView thirdColumn;
    @BindView(R.id.phoneLayout)
    LinearLayout phoneLayout;
    @BindView(R.id.phoneText)
    TextView phoneText;
    @BindView(R.id.emailLayout)
    LinearLayout emailLayout;
    @BindView(R.id.emailText)
    TextView emailText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);

        imagesService.displayImage(R.drawable.my_photo, profileImage);

        firstColumn.setText(setDependenciesText(getResources().getStringArray(R.array.first_column)));
        secondColumn.setText(setDependenciesText(getResources().getStringArray(R.array.second_column)));
        thirdColumn.setText(setDependenciesText(getResources().getStringArray(R.array.third_column)));

        return view;
    }

    private StringBuilder setDependenciesText(String[] list) {
        ArrayList<String> dependencies = new ArrayList<>(Arrays.asList(list));
        StringBuilder dependenciesString = new StringBuilder();
        for (int i = 0; i < dependencies.size(); i++) {
            String dependence = dependencies.get(i);
            dependenciesString.append(dependence).append("\n");
        }
        return dependenciesString;
    }

    @OnClick(R.id.phoneLayout)
    void onPhoneLayoutClick() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneText.getText().toString(), null));
        startActivity(intent);
    }

    @OnClick(R.id.emailLayout)
    void onEmailLayoutClick() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailText.getText().toString()});
        try {
            startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
