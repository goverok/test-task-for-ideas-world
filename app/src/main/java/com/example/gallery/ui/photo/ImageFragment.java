package com.example.gallery.ui.photo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gallery.services.listeners.GetDetailsListener;
import com.google.gson.Gson;
import com.example.gallery.App;
import com.example.gallery.R;
import com.example.gallery.data.ImageData;
import com.example.gallery.services.ImagesInteractor;
import com.example.gallery.services.ImagesService;
import com.example.gallery.services.network.ApiRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageFragment extends Fragment {
    private ImageData image;
    @Inject
    ApiRepository apiRepository;
    @Inject
    ImagesService imagesService;
    @Inject
    ImagesInteractor imagesInteractor;

    @BindView(R.id.image)
    ImageView mainImage;
    @BindView(R.id.time)
    TextView timeText;
    @BindView(R.id.username)
    TextView userText;
    @BindView(R.id.description)
    TextView descriptionText;
    @BindView(R.id.favoriteImage)
    ImageView favoriteImage;
    @BindView(R.id.createdAt)
    TextView createdAt;
    @BindView(R.id.userProfileImage)
    ImageView userProfileImage;
    @BindView(R.id.userProfile)
    LinearLayout userProfile;
    @BindView(R.id.portfolioUrl)
    TextView portfolioUrl;
    @BindView(R.id.portfolioText)
    TextView portfolioText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String jsonGame;
        if (getArguments() != null) {
            jsonGame = getArguments().getString("image");
            image = new Gson().fromJson(jsonGame, ImageData.class);
        }
        App.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        ButterKnife.bind(this, view);

        apiRepository.getDetails(new GetDetailsListener() {
            @Override
            public void onSuccess(ImageData imageData) {
                userProfile.setVisibility(View.VISIBLE);
                imagesService.displayImage(imageData, mainImage);
                imagesService.displayImage(imageData.isFavorite() ? R.drawable.ic_favorite_red_24dp : R.drawable.ic_favorite_border_black_24dp, favoriteImage);
                imagesService.displayUserProfileImage(imageData, userProfileImage);

                if (image.getDate() != null) {
                    timeText.setVisibility(View.VISIBLE);
                    createdAt.setVisibility(View.VISIBLE);
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
                    timeText.setText(dateFormat.format(image.getDate()));
                } else {
                    timeText.setVisibility(View.GONE);
                    createdAt.setVisibility(View.GONE);
                }

                if (image.getDescription() != null) {
                    descriptionText.setVisibility(View.VISIBLE);
                    descriptionText.setText(image.getDescription());
                } else {
                    descriptionText.setVisibility(View.GONE);
                }

                if (image.getUsername() != null) {
                    userText.setVisibility(View.VISIBLE);
                    userText.setText(image.getUsername());
                } else {
                    userText.setVisibility(View.GONE);
                }

                if (image.getUserPortfolioLink() != null) {
                    portfolioText.setVisibility(View.VISIBLE);
                    portfolioUrl.setVisibility(View.VISIBLE);
                    portfolioUrl.setText(image.getUserPortfolioLink());
                } else {
                    portfolioText.setVisibility(View.GONE);
                    portfolioUrl.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }
        }, image);

        favoriteImage.setOnClickListener(v -> {
            if (image.isFavorite()) {
                imagesInteractor.removeFromFavorites(image, () -> {
                });
            } else {
                imagesInteractor.addToFavorites(image, () -> {
                });
            }
            imagesService.displayImage(image, mainImage);
            imagesService.displayImage(image.isFavorite() ? R.drawable.ic_favorite_red_24dp : R.drawable.ic_favorite_border_black_24dp, favoriteImage);
        });

        return view;
    }

    public static ImageFragment newInstance(ImageData data) {
        ImageFragment fragment = new ImageFragment();
        Bundle bundle = new Bundle();
        String jsonData = new Gson().toJson(data);
        bundle.putString("image", jsonData);
        fragment.setArguments(bundle);
        return fragment;
    }
}
